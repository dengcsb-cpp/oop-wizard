#include <iostream>
#include <string>
#include "Spell.h"
#include "Wizard.h"

using namespace std;

void act(Wizard* actor, Wizard* target);

int main()
{
	// Create wizard 1
	Wizard* wiz1 = new Wizard("Gandalf");
	// You can store the spell in a variable first if you prefer
	wiz1->setSpell(new Spell("Blinding Light", wiz1, 50));

	// Create wizard 2
	Wizard* wiz2 = new Wizard("Megumin");
	wiz2->setSpell(new Spell("Explosion", wiz2, 50));

	// Play the game until one is dead
	while (wiz1->alive() && wiz2->alive())
	{
		// Wiz 1's turn
		act(wiz1, wiz2);

		// Wiz 2's turn
		act(wiz2, wiz1);

		system("cls");
	}

	// Battle result
	if (wiz1->alive()) cout << wiz1 << " wins!" << endl;
	else if (wiz2->alive()) cout << wiz2 << "wins!" << endl;
	else cout << "Draw!" << endl;

	return 0;
}

void act(Wizard* actor, Wizard* target)
{
	// Prioritize spell over attack
	if (actor->canCastSpell()) actor->getSpell()->activate(target);
	else actor->attack(target);

	system("pause");
}
