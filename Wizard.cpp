#include "Wizard.h"
#include "Spell.h"

Wizard::Wizard(string name)
{
	mName = name;
	mHp = 250;
	mDamageMin = 10;
	mDamageMax = 15;
	mMpRecoveryMin = 10;
	mMpRecoveryMax = 20;
}

string Wizard::getName()
{
	return mName;
}

int Wizard::getHp()
{
	return mHp;
}

int Wizard::getMp()
{
	return mMp;
}

int Wizard::getDamageMin()
{
	return mDamageMin;
}

int Wizard::getDamageMax()
{
	return mDamageMax;
}

int Wizard::getMpRecoveryMin()
{
	return mMpRecoveryMin;
}

int Wizard::getMpRecoveryMax()
{
	return mMpRecoveryMax;
}

Spell* Wizard::getSpell()
{
	return mSpell;
}

void Wizard::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

bool Wizard::alive()
{
	return mHp > 0;
}

void Wizard::attack(Wizard* target)
{
	// Check if target is set
	if (target == nullptr) return;

	// Apply damage
	int damage = rand() % (mDamageMax - mDamageMin + 1) + mDamageMin;
	target->takeDamage(damage);
	cout << mName << " attacked " << target->getName() << "! " << damage << " damage!" << endl;

	// Recover mp
	int mp = rand() % (mMpRecoveryMax - mMpRecoveryMin + 1) + mDamageMin;
	recoverMp(mp);
	cout << mName << " recovered " << mp << "MP!" << endl;
}

bool Wizard::canCastSpell()
{
	return mMp >= mSpell->getMpCost();
}

void Wizard::setSpell(Spell* spell)
{
	mSpell = spell;
}

void Wizard::useMp(int mp)
{
	if (mp < 1) return;
	mMp -= mp;
	if (mMp < 0) mMp = 0;
}

void Wizard::recoverMp(int mp)
{
	if (mp < 1) return;
	mMp += mp;
}

void Wizard::setAttackDamage(int min, int max)
{
	if (min > max)
	{
		cout << "Invalid parameter" << endl;
		return;
	}

	mDamageMin = min;
	mDamageMax = max;
}

void Wizard::setAttackMpRecovery(int min, int max)
{
	if (min > max)
	{
		cout << "Invalid parameter" << endl;
		return;
	}

	mMpRecoveryMin = min;
	mMpRecoveryMax = max;
}
