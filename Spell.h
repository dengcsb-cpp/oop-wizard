#pragma once
#include <string>
#include <iostream>
using namespace std;

class Wizard;

class Spell
{
public:
	Spell(string name, Wizard* actor, int mpCost);

	string getName();
	int getMpCost();
	int getDamageMin();
	int getDamageMax();

	void setDamageRange(int min, int max);
	void setMpCost(int value);

	void activate(Wizard* target);

private:
	string mName;
	int mMpCost;
	int mDamageMin;
	int mDamageMax;
	Wizard* mActor;
};

