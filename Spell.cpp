#include "Spell.h"
#include "Wizard.h"

Spell::Spell(string name, Wizard* actor, int mpCost)
{
	mName = name;
	mActor = actor;
	mMpCost = mpCost;
	mDamageMin = 40;
	mDamageMax = 60;
}

string Spell::getName()
{
	return mName;
}

int Spell::getMpCost()
{
	return mMpCost;
}

int Spell::getDamageMin()
{
	return mDamageMin;
}

int Spell::getDamageMax()
{
	return mDamageMax;
}

void Spell::setDamageRange(int min, int max)
{
	if (min > max)
	{
		cout << "Invalid parameter" << endl;
		return;
	}

	mDamageMin = min;
	mDamageMax = max;
}

void Spell::setMpCost(int value)
{
	if (value < 0) return;
	mMpCost = value;
}

void Spell::activate(Wizard* target)
{
	// Check if there's a target
	if (target == nullptr) return;

	// Check if mp is enough
	if (mActor->getMp() < mMpCost) return;

	// Consume mp
	mActor->useMp(mMpCost);

	// Apply damage
	int damage = rand() % (mDamageMax - mDamageMin + 1) + mDamageMin;
	target->takeDamage(damage);

	cout << mActor->getName() << " used " << mName << "! " << damage << " damage!" << endl;
}
