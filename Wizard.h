#pragma once
#include <string>
#include <iostream>
using namespace std;

class Spell;

class Wizard
{
public:
	Wizard(string name);

	string getName();
	int getHp();
	int getMp();
	int getDamageMin();
	int getDamageMax();
	int getMpRecoveryMin();
	int getMpRecoveryMax();
	Spell* getSpell();

	void takeDamage(int damage);
	bool alive();

	void attack(Wizard* target);
	bool canCastSpell();
	void setSpell(Spell* spell);
	
	void useMp(int mp);
	void recoverMp(int mp);

	void setAttackDamage(int min, int max);
	void setAttackMpRecovery(int min, int max);

private:
	string mName;
	int mHp;
	int mMp;
	int mDamageMin;
	int mDamageMax;
	int mMpRecoveryMin;
	int mMpRecoveryMax;
	Spell* mSpell;
};

